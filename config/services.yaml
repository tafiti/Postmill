# Learn more about services, parameters and containers at
# http://symfony.com/doc/current/service_container.html

imports:
    - ./ratelimit.yaml

parameters:
    env(TRUSTED_HOSTS): ''
    days_to_keep_logs: 7
    fonts_config: "%env(json:file:resolve:APP_FONTS)%"
    themes_config: "%env(json:file:resolve:APP_THEMES)%"
    ratelimit_ip_whitelist: "%env(csv:RATELIMIT_WHITELIST)%"
    submission_sort_modes: active|hot|new|top|controversial|most_commented
    number_regex: '[1-9][0-9]{0,17}'
    username_regex: '\w{3,25}|!deleted\d+'
    uuid_regex: '[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}'
    wiki_page_regex: '[A-Za-z][A-Za-z0-9_-]*(/[A-Za-z][A-Za-z0-9_-]*)*'

services:
    _defaults:
        autoconfigure: true
        autowire: true
        bind:
            $batchSize: "%env(BATCH_SIZE)%"
            $defaultLocale: "%env(APP_LOCALE)%"
            $fontsConfig: "%fonts_config%"
            $themesConfig: "%themes_config%"
            $secret: "%env(APP_SECRET)%"
            $siteName: '@=service("App\\Repository\\SiteRepository").getCurrentSiteName()'
            $uploadRoot: "%env(resolve:UPLOAD_ROOT)%"
            bool $enableExperimentalRestApi: "%env(bool:ENABLE_EXPERIMENTAL_REST_API)%"
            array $trustedHosts: "%env(csv:TRUSTED_HOSTS)%"
        public: false

    # makes classes in src/ available to be used as services
    # this creates a service per class whose id is the fully-qualified class name
    App\:
        resource: '../src/*'
        # you can exclude directories or files
        # but if a service is unused, it's removed anyway
        exclude: '../src/{DependencyInjection,Entity,Migrations}'

    # controllers are imported separately to make sure they're public
    # and have a tag that allows actions to type-hint services
    App\Controller\:
        resource: '../src/Controller'
        public: true
        tags: ['controller.service_arguments']

    App\Command\ActivateMaintenanceMode:
        $includeFilePath: "%kernel.project_dir%/var/maintenance.php"

    App\EventListener\LocaleListener:
        arguments:
            $availableLocales: "%env(csv:AVAILABLE_LOCALES)%"
        tags:
            - { name: kernel.event_listener, event: kernel.request, priority: 20 }
            - { name: kernel.event_listener, event: security.interactive_login, method: onInteractiveLogin }
            - { name: doctrine.event_listener, event: postUpdate }

    App\Form\UserSettingsType:
        $availableLocales: "%env(csv:AVAILABLE_LOCALES)%"

    App\Mailer\ResetPasswordMailer:
        $noReplyAddress: "%env(NO_REPLY_ADDRESS)%"

    App\Markdown\MarkdownConverter:
        $cacheItemPool: "@cache.markdown"

    App\Message\Handler\DownloadSubmissionImageHandler:
        $httpClient: "@eight_points_guzzle.client.submission_image_client"

    App\Security\AuthenticationHelper:
        $rememberMeServices: "@security.authentication.rememberme.services.simplehash.main"

    App\Security\LoginAuthenticator:
        $rateLimit: "@postmill.ratelimit.login"

    App\Twig\AppExtension:
        $branch: "%env(APP_BRANCH)%"
        $version: "%env(APP_VERSION)%"

    App\Utils\IpRateLimitFactory:
        $cache: "@cache.ratelimit"
        $ipWhitelist: "%ratelimit_ip_whitelist%"

    App\Utils\IpRateLimit:
        class: App\Utils\IpRateLimit
        factory: 'App\Utils\IpRateLimitFactory:create'
        abstract: true

    App\Validator\Constraints\RateLimitValidator:
        $ipWhitelist: "%ratelimit_ip_whitelist%"
